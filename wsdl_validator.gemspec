lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'wsdl_validator/version'

Gem::Specification.new do |spec|
  spec.name          = 'wsdl_validator'
  spec.version       = WsdlValidator::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['Samuel.Garratt@integrationqa.co.nz']

  spec.summary       = %q{Makes verifying a SOAP message against a WSDL easy.}
  spec.description   = %q{Makes verifying a SOAP message against a WSDL easy.}
  spec.homepage      = 'https://gitlab.com/samuel-garratt/wsdl_validator'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.add_dependency 'httpi'
  spec.add_dependency 'nokogiri'
  spec.add_dependency 'wasabi'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rackup'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'savon'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'sinatra'
  spec.add_development_dependency 'sinatra-basic-auth'
end
