require 'simplecov'
SimpleCov.start

require 'bundler/setup'
require 'wsdl_validator'

RSpec.configure do |config|
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # Close test server after all RSpec tests have run
  config.after(:suite) do
    Process.kill(:QUIT, ENV['wsdl_server_pid'].to_i) if ENV['wsdl_server_pid']
  end
end
