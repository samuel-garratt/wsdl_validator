require 'savon'

RSpec.describe 'Savon Response' do
  let(:blz_wsdl) { 'http://localhost:3333/BLZService?wsdl' }
  let(:bank_client) { Savon::Client.new(wsdl: blz_wsdl) }
  it 'Finds error correctly' do
    response = bank_client.call(:get_bank, message: { blz: 500 })
    expect do
      WsdlValidator.new(blz_wsdl).valid?(response.xml)
    end.to raise_error WsdlValidator::Error, /The attribute 'lang' is not allowed/
  end
  it 'allows successful XML' do
    response = bank_client.call(:get_bank, message: { blz: 600 })
    expect(WsdlValidator.new(blz_wsdl).valid?(response.xml)).to be true
  end
end