RSpec.describe WsdlValidator do
  let(:single_schema) { WsdlValidator.new('http://localhost:3333/single_schema?wsdl') }
  let(:namespaces) { single_schema.namespaces }
  it 'Allows one to access namespaces from WSDL' do
    expect(namespaces).to be_instance_of Hash
    expect(namespaces.keys.count).to eq 16
    expect(namespaces['xmlns:xs']).to eq 'http://www.w3.org/2001/XMLSchema'
  end
end