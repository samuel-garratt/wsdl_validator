RSpec.describe 'Importing schema' do
  let(:wsdl_with_import) { WsdlValidator.new('http://admin:secret@localhost:3333/wsdl_with_import?wsdl') }
  let(:soap_xml) do
    File.read(File.join('spec', 'support', 'soap_note.xml'))
  end
  let(:invalid_soap) do
    File.read(File.join('spec', 'support', 'invalid_soap_note.xml'))
  end
  it 'reads imported xsd' do
    expect(wsdl_with_import.show_schemas).to include '<xs:element name="to" type="xs:string"/>'
  end
  it 'parses soap according to imported xsd' do
    expect(wsdl_with_import.valid?(soap_xml)).to be true
  end
end
