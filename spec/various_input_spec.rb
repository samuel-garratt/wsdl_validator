RSpec.describe 'Validator handles' do
  let(:single_schema) { WsdlValidator.new('http://localhost:3333/single_schema?wsdl') }
  let(:node_set_xml) do
    Nokogiri::XML(File.read(File.join('spec', 'support', 'contains_xml_to_check.xml'))).children.children
  end
  let(:single_correct_doc) do
    File.read(File.join('spec', 'support', 'single_schema.xml'))
  end
  let(:single_correct_xml) do
    Nokogiri::XML(single_correct_doc)
  end
  it Nokogiri::XML::Document do
    expect(single_schema.valid?(single_correct_xml)).to be true
  end
  it String do
    expect(single_schema.valid?(single_correct_doc)).to be true
  end
  it Nokogiri::XML::NodeSet do
    expect(single_schema.validate(node_set_xml)).to be true
  end
end