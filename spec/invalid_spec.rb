RSpec.describe 'Correct exceptions' do
  let(:multiple_schema) { WsdlValidator.new('http://localhost:3333/multiple_schema?wsdl') }
  let(:invalid_soap) do
    File.read(File.join('spec', 'support', 'invalid_soap_note.xml'))
  end
  it 'invalid xml' do
    expect do
      multiple_schema.valid?(invalid_soap)
    end.to raise_error WsdlValidator::Error, /invalid_element/
  end
  it 'has correct number of errors' do
    expect(multiple_schema.errors_for(invalid_soap).count).to eq 1
  end
  it 'unauthorized wsdl' do
    expect do
      WsdlValidator.new('http://admin:invalid_secret@localhost:3333/secured_import?wsdl')
    end.to raise_error WsdlValidator::Error, /Unauthorized for basic auth/
  end
end