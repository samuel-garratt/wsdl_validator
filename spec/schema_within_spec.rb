RSpec.describe 'Schema within WSDL' do
  let(:single_schema) { WsdlValidator.new('http://localhost:3333/single_schema?wsdl') }
  let(:multiple_schema) { WsdlValidator.new('http://localhost:3333/multiple_schema?wsdl') }
  let(:single_correct_doc) do
    File.read(File.join('spec', 'support', 'single_schema.xml'))
  end
  let(:soap_xml) do
    File.read(File.join('spec', 'support', 'soap_note.xml'))
  end
  let(:soap_ns_in_envelope) do
    File.read(File.join('spec', 'support', 'soap_namespace_in_envelope.xml'))
  end
  context '.valid?' do
    it 'single schema passed correct XML' do
      expect(single_schema.valid?(single_correct_doc)).to be true
    end
    it 'reads multiple schema wsdl' do
      expect(multiple_schema.valid?(single_correct_doc)).to be true
    end
    it 'handles SOAP message' do
      expect(multiple_schema.valid?(soap_xml)).to be true
    end
    it 'handles SOAP with namespace in envelope' do
      expect(multiple_schema.valid?(soap_ns_in_envelope)).to be true
    end
  end
end
