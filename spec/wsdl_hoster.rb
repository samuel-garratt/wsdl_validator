require 'sinatra'
require 'sinatra/basic_auth'

class GetBank

  class << self
    # Example XML simulating what's returned from SOAP request
    def invalid_response
      <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
      <soapenv:Body>
        <ns1:getBankResponse xmlns:ns1="http://thomas-bayer.com/blz/">
          <ns1:details>
            <ns1:bezeichnung lang="German">Deutsche Bank</ns1:bezeichnung>
            <ns1:bic>DEUTDEMMXXX</ns1:bic>
            <ns1:ort>München</ns1:ort>
            <ns1:plz>Berlin</ns1:plz>
          </ns1:details>
        </ns1:getBankResponse>
      </soapenv:Body>
    </soapenv:Envelope>
      EOF
    end

    def success_response
      <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
      <soapenv:Body>
        <ns1:getBankResponse xmlns:ns1="http://thomas-bayer.com/blz/">
          <ns1:details>
            <ns1:bezeichnung>Deutsche Bank</ns1:bezeichnung>
            <ns1:bic>DEUTDEMMXXX</ns1:bic>
            <ns1:ort>München</ns1:ort>
            <ns1:plz>Berlin</ns1:plz>
          </ns1:details>
        </ns1:getBankResponse>
      </soapenv:Body>
    </soapenv:Envelope>
      EOF
    end

    # @return [String] Error response
    def error_response
      <<-EOF
    <soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope">
      <soapenv:Body>
        <soapenv:Fault>
          <soapenv:Code>
            <soapenv:Value>soapenv:Receiver</soapenv:Value>
          </soapenv:Code>
          <soapenv:Reason>
            <soapenv:Text xml:lang="en-US">BLZ not present</soapenv:Text>
          </soapenv:Reason>
        </soapenv:Fault>
      </soapenv:Body>
    </soapenv:Envelope>
      EOF
    end

    # Return a response based upon the SOAP request
    # @param [String] request XML in request
    def response_for(request)
      soap_action = request.env['HTTP_SOAPACTION']
      return 500, 'Not valid SOAP' unless soap_action
      request_body = request.body.read
      request_body.include?('500') ? invalid_response : success_response
    end
  end
end

class WsdlHoster < Sinatra::Application
  set :port, 3333

  # @param [String] relative_file_name File relative to current location
  # @return [Array] Array to be sent for displaying WSDL for sinatra
  def xml_from_file(relative_file_name)
    [200, { 'Content-Type' => 'text/xml' }, File.read(File.join(File.dirname(__FILE__), 'support', relative_file_name))]
  end

  get '/single_schema' do
    xml_from_file 'single_schema.wsdl'
  end

  get '/multiple_schema' do
    xml_from_file 'multiple_soap_schema.wsdl'
  end

  get '/imported_note' do
    xml_from_file 'note.xsd'
  end

  # This is returned when a query for the WSDL is made
  get '/BLZService' do
    xml_from_file 'bank.wsdl'
  end

  # This is the one being hit by SOAP actions
  post '/BLZService' do
    GetBank.response_for request
  end

  # TODO: This is not working as expected. Update
  authorize do |username, password|
    username == 'admin' && password == 'secret'
  end

  get '/wsdl_with_import' do
    xml_from_file 'importing_schema.wsdl'
  end

  get '/imported_bus' do
    xml_from_file 'bus.xsd'
  end

  protect do
    get '/secured_import' do
      xml_from_file 'importing_schema.wsdl'
    end  
  end
end

WsdlHoster.run!
