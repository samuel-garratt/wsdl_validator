class WsdlValidator
  # Raised to represent an error occurred when parsing an WSDL
  class Error < StandardError
    def initialize(msg = 'Invalid Wsdl')
      super
    end
  end
end