# WsdlValidator

A simple gem to aid with validator a SOAP message according to a WSDL.

[![Build Status](https://gitlab.com/samuel-garratt/wsdl_validator/badges/master/build.svg)](https://gitlab.com/samuel-garratt/wsdl_validator/pipelines)

This is a work in progress. If your xml is not being validated corrected please raise an issue.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wsdl_validator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install wsdl_validator

## Usage

```ruby
wsdl = WsdlValidator.new('http://localhost:3333/single_schema?wsdl')
wsdl.valid? xml_message
```
=> Returns true or raises an exception

## Todo

Validate

* Including the SOAP header/body message structure.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/samuel-garratt/wsdl_validator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the WsdlValidator project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/wsdl_validator/blob/master/CODE_OF_CONDUCT.md).
